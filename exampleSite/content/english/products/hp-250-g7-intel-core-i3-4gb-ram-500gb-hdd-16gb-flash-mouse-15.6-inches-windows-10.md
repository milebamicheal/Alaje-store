+++
colors = []
date = ""
description = "HP 250 G7"
discount_price = ""
image = "/images/1.jpg"
price = "165,000"
sizes = []
title = "HP 250 G7 Intel Core i3 (4GB RAM + 500GB HDD) 16gb Flash + Mouse 15.6 inches Windows 10 "

+++
* **Processor Type**: Intel Core i3
* **Display Features**: Full HD
* **Display Size (inches)**: 15.6
* **Features**: Bluetooth|Wi-Fi| Camera|DVD RW
* **Hard Disk (GB)**: 500
* **Operating System**: Windows 10
* **Internal Memory(GB)**: 4
* **Model**: HP 250
* **Size (L x W x H cm)**: 14.8"W x 9.7"D x 0.9"H
* **Weight (kg)**: 2.19