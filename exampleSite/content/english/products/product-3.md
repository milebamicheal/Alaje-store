---
title: Notebook 15 Intel Core I3 (4GB RAM, 500GB HDD) 32GB Flash+Mouse+ 15.6-Inch
  Windows 10 Black Colour
date: 2019-10-17T11:22:16.000+06:00
images:
- images/showcase/showcase-3.png
- images/showcase/showcase-2.png
- images/showcase/showcase-1.png
- images/showcase/showcase-4.png
description: HP Notebook 15
price: '150,000.00'
discount_price: '145,000.00'
colors: []
sizes: []
image: "/images/3xy16ea-hp-15-notebook.png"

---
* Intel® Core™ i3-5005U – 3xy28ea (2 GHz, 3 MB cache, 2 cores)
* 15.6″ diagonal HD SVA anti-glare WLED-backlit (1366 x 768) Display
* 4 GB DDR3L-1600 SDRAM (1 x 4 GB), 500 GB 5400 rpm SATA
* Integrated Intel® HD Graphics 5500
* DVD-Writer, Full-size island-style keyboard with numeric keypad
* 802.11b/g/n (1×1) Wi-Fi® and Bluetooth® 4.0 combo
* 1 multi-format SD media card reader
* 3-cell Battery
* Dual speakers
* Windows 10
* 1 Year Warranty.