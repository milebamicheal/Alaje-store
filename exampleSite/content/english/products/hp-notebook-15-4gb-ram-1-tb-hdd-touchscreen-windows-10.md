+++
colors = []
date = ""
description = "HP Notebook 15"
discount_price = "145,000.00"
image = "/images/dw3069.jpg"
price = "150,000.00"
sizes = []
title = "HP Notebook 15 (4GB RAM + 1 TB HDD) TouchScreen Windows 10"

+++
* Intel® Core™ i3-8130U (2.2 GHz base frequency, up to 3.4 GHz with Intel® Turbo Boost Technology, 4 MB cache, 2 cores),
* 15.6″) diagonal HD SVA BrightView micro-edge WLED-backlit touch screen (1366 x 768) Display,
* 4 GB DDR4-2400 SDRAM (1 x 4 GB),
* 1 TB 5400 rpm SATA,
* Intel® UHD Graphics 620,
* Optical drive not included,
* Realtek RTL8821CE 802.11b/g/n/ac (1×1) and Bluetooth® 4.2 Combo,
* 1 multi-format SD media card reader,
* 3-cell battery,
* Dual speakers
* 1 Year Warranty
* Windows 10