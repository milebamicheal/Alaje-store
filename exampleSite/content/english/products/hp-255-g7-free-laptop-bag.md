+++
colors = []
date = ""
description = ""
discount_price = "102,000"
image = "/images/portatil_6hl25ea_01_l.jpg"
price = "110,000"
sizes = []
title = "HP 255 G7 (FREE LAPTOP BAG)"

+++
* **Processor Type**: AMD Radeon M4
* **Display Features**: Full HD
* **Display Size (inches)**: 15.6
* **Features**: Bluetooth|Wi-Fi| Camera|DVD RW
* **Hard Disk (GB)**: 500
* **Operating System**: Windows 10
* **Internal Memory(GB)**: 6
* **Model**: HP 250
* **Product Line**: paragon technologies ltd
* **Size (L x W x H cm)**: 14.8"W x 9.7"D x 0.9"H
* **Weight (kg)**: 2.19