---
title: Contact
description: 'Contact Us for more information '
office:
  title: Central Office
  mobile: '07080915046'
  email: info@alaje.com
  location: 'Lagos, Nigeria '
  content: You can visit any our offices
opennig_hour:
  title: Opening Hours
  day_time:
  - 'Monday: 8:00 am – 6:00 pm'
  - 'Tuesday: 8:00 am –  6:00 pm'
  - 'Wednesday: 8:00 am – 6:00 pm'
  - 'Thursday: 8:00 am – 6:00 pm'
  - 'Friday: 8:00 am – 6:00 pm'
  - 'Saturday: 9:00 am – 6:00 pm'

---
